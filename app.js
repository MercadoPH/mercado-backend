const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const app = express(feathers())
const port = 3003

app.use(express.json())

app.use(express.urlencoded({ extended: true}));

app.configure(express.rest())

app.use(express.errorHandler())

const server = app.listen(port)


app.post('/buyUMAC', (req, res) => {
    res.send({
        text: 'buyUMAC'
    })
})

app.post('/withdraw', (req, res) => {
    res.send({
        text: 'withdraw'
    })
})

server.on('listening', () => {
    console.log(` Listening to port ${port}`)
})



