pragma solidity ^0.4.23;

contract UMACToken {
    string name;
    string symbol;
    uint decimals;
    uint supply;
    uint supplyUsed;
    mapping (address => uint256) balances;
    uint ratePerEth;
    
    event Transfer(uint recipientNewBalance, uint senderNewBalance, uint amount);
    event Withdraw(uint amountWithdraw, uint senderNewBalance);
    event BuyTokens(uint tokenBought, uint supplyBought);
    
    constructor() public {
        name = 'UMACToken';
        symbol = 'UMAC';
        // decimals = 18;
        // supply = 1000000 * (10 ** 18);
        supplyUsed = 0;
        ratePerEth = 1 ether;
    }
    
    modifier enoughBalanceToTransfer(uint256 value) {
        require(balances[msg.sender] >= value);
        _;
    } 
    
    modifier notNegativeAndZero(uint256 value) {
        require(0 < value);
        _;
    }
    
    function totalSupply() public view returns (uint256) {
        // return supply + supplyUsed; // total supply 
        return supplyUsed;
    }
    
    function balanceOf(address who) public view returns (uint256) {
        return balances[who];
    }
    
    function transfer(address to, uint256 value) public enoughBalanceToTransfer(value) notNegativeAndZero(value) {
        balances[to] += value;
        balances[msg.sender] -= value;
        emit Transfer(balances[to], balances[msg.sender], value);
    }
    
    function withdraw(uint amount) public enoughBalanceToTransfer(amount) notNegativeAndZero(amount) {
        balances[msg.sender] -= amount; 
        msg.sender.transfer(amount * 1 ether);
        emit Withdraw(amount, balances[msg.sender]);
    }
    
    function buyTokens() payable public {
        require(msg.value > 0);
        uint tokenBought = msg.value / (ratePerEth);
        balances[msg.sender] += tokenBought;
        supplyUsed += tokenBought;
        emit BuyTokens(tokenBought, supplyUsed);
        // supply -= tokenBought;
    }
}

contract UMACMarketPlace is UMACToken {
    struct Product {
        uint id;
        string name;
        string description;
        address owner;
        address highestBidder;
        uint highestBid;
        uint startTime;
        uint durationTime;
        string imageURL;
        bool available;
        mapping(address => uint) refunds;
    }
    
    Product[] public products;
    mapping (address => uint[]) public userProducts;
    mapping (address => uint[]) public productsBought;
    mapping (address => mapping(address => uint)) public investment;
    mapping (address => mapping(address => uint)) public shares;
    
    event Invest(uint amount);
    event Share(uint amount);
    event AddProduct(string name, string description, uint startTime, uint endTime, uint id, address owner);
    event Bid(address highestBidder, uint highestBid);
    event EndBid(bool availability);
    event GetRefund(uint balance);
    
    modifier higherThanHighestBid(uint index, uint amount) {
        require(products[index].highestBid < amount);
        _;
    }
    
    modifier onlyOwnProduct(uint index) {
        require(products[index].owner == msg.sender);
        _;
    }
    
    modifier productAvailable(uint index) {
        require(products[index].available == true);
        _;
    }
    
    function invest(uint amount, address farmer) public enoughBalanceToTransfer(amount) notNegativeAndZero(amount) {
        investment[msg.sender][farmer] += amount;
        balances[msg.sender] -= amount;
        emit Invest(investment[msg.sender][farmer]);
    }
    
    function share(uint amount, address investor) public enoughBalanceToTransfer(amount) notNegativeAndZero(amount) {
        shares[msg.sender][investor] += amount;
        balances[msg.sender] -= amount;
        emit Share(investment[msg.sender][investor]);
    }
    
    function addProduct(string _name, string _description, string _dateType, uint _numberOfTime) public {
        Product memory currentProduct;
        currentProduct.name = _name;
        currentProduct.description = _description;
        if(keccak256(_dateType) == keccak256('days')) {
           currentProduct.durationTime  = _numberOfTime * 1 days;
        } else if(keccak256(_dateType) == keccak256('hours')) {
            currentProduct.durationTime = _numberOfTime * 1 hours;
        } else if(keccak256(_dateType) == keccak256('minutes')) {
            currentProduct.durationTime = _numberOfTime * 1 minutes;
        }
        currentProduct.owner = msg.sender;
        currentProduct.startTime = now;
        currentProduct.available = true;
        uint newId = products.push(currentProduct) - 1;
        currentProduct.id = newId;
        userProducts[msg.sender].push(newId);
        emit AddProduct
            (
                currentProduct.name,
                currentProduct.description,
                currentProduct.startTime,
                currentProduct.durationTime,
                currentProduct.id,
                currentProduct.owner
            );
        
    }
    
    function bid(uint index, uint amount) public 
        enoughBalanceToTransfer(amount) notNegativeAndZero(amount) higherThanHighestBid(index, amount) productAvailable(index){
        products[index].refunds[products[index].highestBidder] +=  products[index].highestBid;
        products[index].highestBidder = msg.sender;
        products[index].highestBid = amount;
        balances[msg.sender] -= amount;
        emit Bid(products[index].highestBidder, products[index].highestBid);
    }
    
    function endBid(uint index) public 
        onlyOwnProduct(index) productAvailable(index)
    {
        products[index].available = false;
        balances[msg.sender] += products[index].highestBid;
        products[index].highestBid = 0;
        productsBought[products[index].highestBidder].push(products[index].id);
        emit EndBid(products[index].available);
    }
    
    function getRefund(uint index) public {
        balances[msg.sender] += products[index].refunds[msg.sender];
        products[index].refunds[msg.sender] =  0;
        emit GetRefund(balances[msg.sender]);
    }

    function getProduct(uint index) public view returns 
        (
            string,
            string,
            address,
            address,
            uint,
            uint,
            uint,
            string,
            bool
        )
    {
        Product memory currenctProduct = products[index];
        return
        (
         currenctProduct.name,
         currenctProduct.description,   
         currenctProduct.owner,   
         currenctProduct.highestBidder,   
         currenctProduct.highestBid,   
         currenctProduct.startTime,  
         currenctProduct.durationTime,  
         currenctProduct.imageURL,
         currenctProduct.available
        );    
    }
}